//
//  ViewController.swift
//  RGBProject
//
//  Created by Amir on 09.03.2022.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var redLabel: UILabel!
    @IBOutlet weak var greenLabel: UILabel!
    @IBOutlet weak var blueLabel: UILabel!
    
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    
    @IBOutlet weak var redValueTF: UITextField!
    @IBOutlet weak var greenValueTF: UITextField!
    @IBOutlet weak var blueValueTF: UITextField!
    
    //MARK: - Public Properties
    var backgroundColor: UIColor!
    var delegate: SettingsViewControllerDelegare!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colorView.layer.cornerRadius = 15
        setSliders()
        
        setValue(for: redLabel, greenLabel, blueLabel)
        setValue(for: redValueTF, greenValueTF, blueValueTF)
        
        redValueTF.delegate = self
        greenValueTF.delegate = self
        blueValueTF.delegate = self
        
        
    }

    @IBAction func rgbSlider(_ sender: UISlider) {
        backgroundColorSetter()
        switch sender{
        case redSlider:
            redLabel.text = String(rounder(redSlider.value))
            redValueTF.text = String(rounder(redSlider.value))
        case greenSlider:
            greenLabel.text = String(rounder(greenSlider.value))
            greenValueTF.text = String(rounder(greenSlider.value))
        default:
            blueLabel.text = String(rounder(blueSlider.value))
            blueValueTF.text = String(rounder(blueSlider.value))
        }
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        view.endEditing(true)
        guard let color = colorView.backgroundColor else {return}
        delegate.setColor(color)
        dismiss(animated: true)
    }
    
}

//MARK: -  Private functions
extension SettingsViewController {
    private func setValue(for labels: UILabel...) {
        labels.forEach { label in
            switch label {
            case redLabel:
                redLabel.text = String(rounder(redSlider.value))
            case greenLabel:
                greenLabel.text = String(rounder(greenSlider.value))
            default:
                blueLabel.text = String(rounder(blueSlider.value))
            }
        }
    }
    
    private func setValue(for textFields: UITextField...) {
        textFields.forEach { textField in
            switch textField {
            case redValueTF: textField.text = String(redSlider.value)
            case greenValueTF: textField.text = String(greenSlider.value)
            default: textField.text = String(blueSlider.value)
            }
        }
    }
    
    private func backgroundColorSetter(){
        colorView.backgroundColor = UIColor(
            red: CGFloat(redSlider.value),
            green: CGFloat(greenSlider.value),
            blue: CGFloat(blueSlider.value),
            alpha: CGFloat(1.0)
        )
        
    }
    
    private func rounder(_ value: Float) -> Float{
        (round(value * 100) / 100.0)
    }
    
    private func setSliders() {
        let ciColor = CIColor(color: backgroundColor)
        
        redSlider.value = Float(ciColor.red)
        greenSlider.value = Float(ciColor.green)
        blueSlider.value = Float(ciColor.blue)
    }
    
    @objc private func didTapDone() {
        view.endEditing(true)
    }
}


//MARK:-  UITextViewDelegate
extension SettingsViewController: UITextFieldDelegate {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let newValue = textField.text else { return }
        if let currentValue = Float(newValue) {
            switch textField {
            case redValueTF:
                redSlider.setValue(currentValue, animated: true)
                setValue(for: redLabel)
            case greenValueTF:
                greenSlider.setValue(currentValue, animated: true)
                setValue(for: greenLabel)
            default:
                blueSlider.setValue(currentValue, animated: true)
                setValue(for: blueLabel)
            }
            backgroundColorSetter()
            return
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        textField.inputAccessoryView = keyboardToolbar
        
        let doneButton = UIBarButtonItem(
            barButtonSystemItem: .done,
            target: self,
            action: #selector(didTapDone)
        )
        
        let flexBarButton = UIBarButtonItem(
            barButtonSystemItem: .flexibleSpace,
            target: nil,
            action: nil)
        
        keyboardToolbar.items = [flexBarButton, doneButton]
    }
}
