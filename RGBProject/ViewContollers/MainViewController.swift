//
//  MainViewController.swift
//  RGBProject
//
//  Created by Amir on 20.03.2022.
//

import UIKit

protocol SettingsViewControllerDelegare {
    func setColor(_ backgroundColor: UIColor)
}

class MainViewController: UIViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let settingVC = segue.destination as? SettingsViewController else {return}
        settingVC.delegate = self
        settingVC.backgroundColor = view.backgroundColor
        
    }
   

}
//MARK: - Color Delegate
extension MainViewController: SettingsViewControllerDelegare {
    func setColor(_ backgroundColor: UIColor) {
        view.backgroundColor = backgroundColor
    }
}
